package top.codef.exceptionhandle.event;

import java.util.concurrent.Executor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import top.codef.common.abstracts.AbstractNoticeSendListener;
import top.codef.exceptionhandle.interfaces.NoticeStatisticsRepository;
import top.codef.notice.NoticeComponentFactory;
import top.codef.properties.frequency.NoticeFrequencyStrategy;
import top.codef.text.NoticeTextResolverProvider;

public class ExceptionNoticeAsyncSendListener extends AbstractNoticeSendListener {

	private static final Log logger = LogFactory.getLog(ExceptionNoticeAsyncSendListener.class);

	private final Executor executor;

	/**
	 * @param noticeFrequencyStrategy
	 * @param exceptionNoticeStatisticsRepository
	 * @param resolverProvider
	 * @param noticeComponentFactory
	 * @param resolverName
	 * @param executor
	 */
	public ExceptionNoticeAsyncSendListener(NoticeFrequencyStrategy noticeFrequencyStrategy,
			NoticeStatisticsRepository exceptionNoticeStatisticsRepository, NoticeTextResolverProvider resolverProvider,
			NoticeComponentFactory noticeComponentFactory, String resolverName, Executor executor) {
		super(noticeFrequencyStrategy, exceptionNoticeStatisticsRepository, resolverProvider, noticeComponentFactory);
		this.executor = executor;
	}

	@Override
	public void onApplicationEvent(ExceptionNoticeEvent event) {
		logger.debug("异步发送消息");
		executor.execute(() -> send(event.getBlameFor(), event.getExceptionNotice()));
	}

}
